import argparse
import random

parser = argparse.ArgumentParser(description='Parsing Password')
parser.add_argument("--length", required=True, default=1, type=int, help="Length of the password")
parser.add_argument("--include", required=False, type=str, help="To include the word in the password")

args = parser.parse_args()

password_length = args.length

alpha_upper = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
               "V", "W", "X", "Y", "Z"]
alpha_lower = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
               "v", "w", "x", "y", "z"]
numbered = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
special_characters = ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "`", "~", "+", "<", ">"]

combined_alpha_list = alpha_upper + alpha_lower

combined_list = []

counter = 0

while counter < 2:
    combined_list.extend(random.sample(numbered, 1))
    counter = counter + 1
    combined_list.extend(random.sample(special_characters, 1))
    counter = counter + 1

alpha_characters_needed = int(password_length) - 2

random_alpha_characters = (random.sample(combined_alpha_list, alpha_characters_needed))

combined_list.extend(random_alpha_characters)

print(random.sample(combined_list, password_length))
